export const atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

export const btoa = globalThis.btoa || ((src: string) => {
    return Buffer.from(src, 'binary').toString('base64');
})

export function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}

export function textToUint8(text: string): Uint8Array {
    return Uint8Array.from(text, c => c.charCodeAt(0));
}

export function uint8ToText(bytes: Uint8Array): string {
    return Array.from(bytes).map(b => String.fromCharCode(b)).join('');
}

export function uint8ToB64(bytes: Uint8Array): string {
    return btoa(uint8ToText(bytes));
}