import type { SuggestedParams } from "algosdk";
import { AlgoSdk } from "./algosdk";
import { textToUint8 } from "./utils";

export class DripTransactions {

    constructor(
        private algosdk: AlgoSdk,
        private appId: bigint,
        private dripAssetId: bigint,
    ) { }

    optInToAsset(address: string, suggestedParams: SuggestedParams) {
        return this.algosdk.makeAssetTransferTxnWithSuggestedParamsFromObject({
            assetIndex: Number(this.dripAssetId),
            amount: 0,
            from: address,
            to: address,
            suggestedParams,
        });
    }

    optInToDrip(address: string, suggestedParams: SuggestedParams) {
        return this.algosdk.makeApplicationOptInTxnFromObject({
            suggestedParams,
            from: address,
            appIndex: Number(this.appId),
            foreignAssets: [Number(this.dripAssetId)]
        });
    }

    approveAccount(approverAddr: string, addrToApprove: string, suggestedParams: SuggestedParams) {
        return this.algosdk.makeApplicationCallTxnFromObject({
            suggestedParams,
            from: approverAddr,
            appIndex: Number(this.appId),
            onComplete: this.algosdk.OnApplicationComplete.NoOpOC,
            appArgs: [
                textToUint8('allowlist'),
                this.algosdk.decodeAddress(addrToApprove).publicKey,
            ]
        });
    }

    claim(claimerAddr: string, suggestedParams: SuggestedParams) {
        return this.algosdk.makeApplicationCallTxnFromObject({
            suggestedParams: {
                ...suggestedParams,
                fee: 2e3, // TODO: what if fees exceed the minimum of 1e3 microAlgos
                flatFee: true,
            },
            from: claimerAddr,
            appIndex: Number(this.appId),
            onComplete: this.algosdk.OnApplicationComplete.NoOpOC,
            appArgs: [textToUint8('claim')],
            foreignAssets: [Number(this.dripAssetId)]
        });
    }

    optOut(address: string, suggestedParams: SuggestedParams) {
        return this.algosdk.makeApplicationClearStateTxnFromObject({
            appIndex: Number(this.appId),
            from: address,
            suggestedParams
        });
    }
}
