import type algosdk from 'algosdk';

// A type representing the default export of the 'algosdk' module
export type AlgoSdk = typeof algosdk;