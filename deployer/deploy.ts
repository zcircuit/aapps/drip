import { Account, Algodv2, Indexer, makeApplicationCreateTxnFromObject, makeAssetTransferTxnWithSuggestedParamsFromObject, makePaymentTxnWithSuggestedParamsFromObject, OnApplicationComplete, SuggestedParams, waitForConfirmation } from "algosdk";
import { readJson, pathExists } from "fs-extra";
import { join } from 'path';

const configKeyToTemplateSymbol = new Map([
    ['assetId', 'ASSET_ID'],
    ['approverAddr', 'APPROVER_ADDR'],
    ['rewardRate', 'REWARD_RATE'],
    ['maxClaimAccrualSecs', 'MAX_CLAIM_ACC'],
    ['maxClaimPercent', 'MAX_CLAIM_PERCENT'],
]);

const ALGO_ASSET_ID = 1n;
export async function getDeployDripTransaction(
    creatorAddr: string,
    algod: Algodv2,
    suggestedParams: SuggestedParams,
    config: DripDeployConfig,
) {
    if (!config.assetId) config.assetId = ALGO_ASSET_ID;

    const compiledDripFile1 = join(__dirname, './dist/compiledDrip.json');

    // Once packaged the filepath is slightly different
    const compiledDripFile2 = join(__dirname, './compiledDrip.json');
    const actualFile = await pathExists(compiledDripFile1) ? compiledDripFile1 : compiledDripFile2;

    const { appTeal, clear } = await readJson(actualFile);

    const deTemplatedAppTeal = Object.entries(config).reduce((templatedTeal, [key, value]) => {
        return templatedTeal.replace(new RegExp(`TMPL_${configKeyToTemplateSymbol.get(key)}`, 'g'), value);
    }, appTeal);

    const compiledApp = await (algod.compile(deTemplatedAppTeal).do() as any);

    return makeApplicationCreateTxnFromObject({
        approvalProgram: b64ToUint8(compiledApp.result),
        clearProgram: b64ToUint8(clear.result),
        numGlobalInts: 1,
        numGlobalByteSlices: 1,
        numLocalInts: 2,
        numLocalByteSlices: 0,
        onComplete: OnApplicationComplete.NoOpOC,
        from: creatorAddr,
        suggestedParams,
    });
}

export async function deployDrip(algod: Algodv2, indexer: Indexer, creator: Account, config: DripDeployConfig) {
    const suggestedParams = await algod.getTransactionParams().do();
    const dripTx = await getDeployDripTransaction(creator.addr, algod, suggestedParams, config);

    const signedDripTx = dripTx.signTxn(creator.sk);
    await algod.sendRawTransaction(signedDripTx).do();
    await waitForConfirmation(algod, dripTx.txID(), 20);

    const createdDripTransaction = await tryQueryForTransaction(indexer, dripTx.txID());
    const dripAppId = createdDripTransaction.transaction["created-application-index"];
    console.log(`Drip Application created: ${dripAppId}, be sure to fund the faucet to maintain minimum balance`);
    return dripAppId;
}

export async function sendAlgos(algod: Algodv2, from: Account, toAddr: Account['addr'], amountMicroAlgos: number) {
    const suggestedParams = await algod.getTransactionParams().do();

    const tx = makePaymentTxnWithSuggestedParamsFromObject({
        amount: amountMicroAlgos,
        from: from.addr,
        to: toAddr,
        suggestedParams,
    });
    const signedTx = tx.signTxn(from.sk);
    await algod.sendRawTransaction(signedTx).do();
    await waitForConfirmation(algod, tx.txID(), 20);
}

export async function sendAsset(algod: Algodv2, assetId: number, from: Account, toAddr: Account['addr'], amount: number) {
    const suggestedParams = await algod.getTransactionParams().do();

    const tx = makeAssetTransferTxnWithSuggestedParamsFromObject({
        assetIndex: assetId,
        amount,
        from: from.addr,
        to: toAddr,
        suggestedParams,
    });
    const signedTx = tx.signTxn(from.sk);
    await algod.sendRawTransaction(signedTx).do();
    await waitForConfirmation(algod, tx.txID(), 20);
}

const atob = (src: string) => Buffer.from(src, 'base64').toString('binary');
function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(atob(b64), c => c.charCodeAt(0));
}
function tick(time: number) {
    return new Promise(res => setTimeout(res, time));
}

async function tryQueryForTransaction(indexer: Indexer, txid: string, retries = 10) {
    let i = 0;
    let lastError: any;
    while (i < retries) {
        const txn = await indexer.lookupTransactionByID(txid).do().catch(e => {
            lastError = e;
            return null;
        }) as any;

        // Short-circuit on success
        if (txn) return txn;
        await tick(1000);
        i++;
    }

    throw new Error(lastError.toString());
}

export interface DripDeployConfig {
    assetId?: bigint,
    approverAddr: string,
    rewardRate: bigint,
    maxClaimAccrualSecs: bigint,
    maxClaimPercent: bigint,
}
