import { Algodv2, Indexer, mnemonicToSecretKey } from 'algosdk';
import { deployDrip } from '../deployer/deploy';

const algod = new Algodv2(
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'http://localhost',
    4001,
);

const indexer = new Indexer(
    'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
    'http://localhost',
    8980,
);

const mnemonic = 'enforce drive foster uniform cradle tired win arrow wasp melt cattle chronic sport dinosaur announce shell correct shed amused dismiss mother jazz task above hospital';
const account = mnemonicToSecretKey(mnemonic);

(async () => {

    await deployDrip(algod, indexer, account, {
        approverAddr: account.addr,
        assetId: 1n,
        maxClaimAccrualSecs: 60n,
        maxClaimPercent: 60n,
        rewardRate: 60n,
    });
})();

