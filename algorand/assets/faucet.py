from pyteal import *
from utils import *
from parse_params import parse_params
import sys
sys.path.append('..')

MAX_PERCENT = 1_000_000

templatedParams = {
    # - assetId: The asset this faucet will be dispensing
    "ASSET_ID": Tmpl.Int("TMPL_ASSET_ID"),

    # - authorizer: A 32byte encoded address of the account that will act as authorizer when accounts attempt to join the faucet
    "APPROVER_ADDR": Tmpl.Addr("TMPL_APPROVER_ADDR"),

    # - rewardsPerSecond: A number (x/1,000,000) representing what percent of the reserve should be distributed each second
    #    - Assets with a low divisibility or a very low max balance could result in significant 'dust'
    #    - Because of integer math, x will also represent the max number of participants
    "REWARD_RATE": Tmpl.Int("TMPL_REWARD_RATE"),

    # - maxClaimAccumulationSeconds: If we allow a user to accumulate rewards for exceptionally long times, it could result in a drastic shock to the reserve or even depleting it prematurely
    #    - The accumulation effects can be exacerbated by a relatively high rewardsPerSecond (high inflation)
    #    - This param is important to limit the faucet funds that can be idly accumulated
    "MAX_CLAIM_ACC": Tmpl.Int("TMPL_MAX_CLAIM_ACC"),

    # - maxClaimPercent: Largest claim that could be made, as a percent (x/1,000,000) of the current reserve
    #    - Similar mitigation to the previous, to prevent a single claim from depleting the reserve
    #    - Setting this number too high could result in more dust accumulation at end-of-life
    "MAX_CLAIM_PERCENT": Tmpl.Int("TMPL_MAX_CLAIM_PERCENT"),
}

IS_ASSET_FAUCET = ScratchVar(TealType.uint64) # will be overwritten before handling Conditions

def onAppCreate():
    return Seq([
        App.globalPut(Bytes('numP'), Int(0)),
        Approve(),
    ])


def optInToFaucetAsset():
    return Seq([
        Assert(IS_ASSET_FAUCET.load()),
        InnerTxnBuilder.Begin(),
        InnerTxnBuilder.SetField(
            TxnField.type_enum, TxnType.AssetTransfer),
        InnerTxnBuilder.SetField(
            TxnField.xfer_asset, templatedParams["ASSET_ID"]),
        InnerTxnBuilder.SetField(TxnField.asset_amount, Int(0)),
        InnerTxnBuilder.SetField(
            TxnField.asset_receiver, Global.current_application_address()),
        InnerTxnBuilder.SetField(TxnField.fee, Int(0)),
        InnerTxnBuilder.Submit(),
        Approve()
    ])


def userOptIn():
    faucetAsset = AssetHolding.balance(
        Txn.sender(), templatedParams["ASSET_ID"])

    allowedAddr = App.globalGetEx(Int(0), Bytes("allowed"))
    approvedViaAllowlist = If(
        allowedAddr.hasValue()
    ).Then(
        # Important that this is conditionally evaluated
        Txn.sender() == allowedAddr.value()
    ).Else(Int(0))

    return Seq([
        allowedAddr,
        Assert(approvedViaAllowlist),
        If(IS_ASSET_FAUCET.load()).Then(Seq([
            faucetAsset,
            Assert(faucetAsset.hasValue())
        ])), # User must opt-in to the faucet asset
        plusPlus(Bytes('numP')),
        App.localPut(
            Txn.sender(), Bytes("lastClaim"), Global.latest_timestamp()),
        App.localPut(
            Txn.sender(), Bytes("lastNumP"), App.globalGet(Bytes("numP"))),
        Approve(),
    ])


def allowlist():
    return Seq([
        Assert(Txn.sender() == templatedParams["APPROVER_ADDR"]),
        App.globalPut(Bytes("allowed"), Txn.application_args[1]),
        Approve(),
    ])


def claim():
    startTimeSecs = App.localGet(Txn.sender(), Bytes("lastClaim"))
    endTimeSecs = Global.latest_timestamp()
    startParticipants = App.localGet(Txn.sender(), Bytes("lastNumP"))
    endParticipants = App.globalGet(Bytes('numP'))
    appAccountAssetHoldings = AssetHolding.balance(
        Global.current_application_address(), templatedParams["ASSET_ID"])

    availableAlgos = Balance(Global.current_application_address()) - MinBalance(Global.current_application_address())
    currentReserve = If(IS_ASSET_FAUCET.load()).Then(
        appAccountAssetHoldings.value()
    ).Else(availableAlgos)
    
    rewardRateNumerator = Itob(templatedParams["REWARD_RATE"])
    timeDelta = Itob(min(
        endTimeSecs - startTimeSecs,
        templatedParams["MAX_CLAIM_ACC"]
    ))
    aggParticipants = Itob(startParticipants + endParticipants)

    top = BytesMul(
        BytesMul(timeDelta, rewardRateNumerator),
        BytesMul(aggParticipants, Itob(currentReserve)),
    )

    bot = BytesMul(
        Itob(Int(MAX_PERCENT * 2)),
        BytesMul(
            Itob(startParticipants),
            Itob(endParticipants),
        )
    )

    rewards = Btoi(BytesDiv(top, bot))
    maxRewardAsPercentOfReserve = (
        (currentReserve * templatedParams["MAX_CLAIM_PERCENT"]) / Int(MAX_PERCENT))
    finalAmount = min(rewards, maxRewardAsPercentOfReserve)

    return Seq([
        appAccountAssetHoldings,
        InnerTxnBuilder.Begin(),

        If(IS_ASSET_FAUCET.load()).Then(Seq([
            InnerTxnBuilder.SetField(TxnField.type_enum, TxnType.AssetTransfer),
            InnerTxnBuilder.SetField(
                TxnField.xfer_asset, templatedParams["ASSET_ID"]),
            InnerTxnBuilder.SetField(TxnField.asset_amount, finalAmount),
            InnerTxnBuilder.SetField(TxnField.asset_receiver, Txn.sender()),
        ])).Else(Seq([
            InnerTxnBuilder.SetField(TxnField.type_enum, TxnType.Payment),
            InnerTxnBuilder.SetField(TxnField.amount, finalAmount),
            InnerTxnBuilder.SetField(TxnField.receiver, Txn.sender()),
        ])),

        InnerTxnBuilder.SetField(TxnField.fee, Int(0)),
        InnerTxnBuilder.Submit(),

        App.localPut(
            Txn.sender(), Bytes("lastClaim"), Global.latest_timestamp()),
        App.localPut(
            Txn.sender(), Bytes("lastNumP"), App.globalGet(Bytes("numP"))),
        Approve()
    ])


def faucet():

    neverAllowedConditions = [
        Txn.on_completion() == OnComplete.DeleteApplication,
        Txn.on_completion() == OnComplete.UpdateApplication,
        Txn.on_completion() == OnComplete.CloseOut,
    ]

    return Seq([
        # Important that this is evaluated JIT so the value can be templated by the 
        #   deployer package
        IS_ASSET_FAUCET.store(templatedParams["ASSET_ID"] != Int(1)),

        Cond(
            [Or(*neverAllowedConditions), Reject()],

            [Txn.application_id() == Int(0), onAppCreate()],
            [Txn.on_completion() == OnComplete.OptIn, userOptIn()],
            [Txn.application_args[0] == Bytes("claim"), claim()],
            [Txn.application_args[0] == Bytes("aoptin"), optInToFaucetAsset()],
            [Txn.application_args[0] == Bytes("allowlist"), allowlist()],
            [Int(1), Reject()],
        )
    ])


if __name__ == "__main__":
    # Overwrite params if sys.argv[1] is passed
    if(len(sys.argv) > 1):
        templatedParams = parse_params(sys.argv[1], templatedParams)

    print(compileTeal(faucet(), Mode.Application, version=6))
