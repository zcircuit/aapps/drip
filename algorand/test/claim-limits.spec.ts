import algosdk, { Account } from "algosdk";
import { assert } from "chai";
// import { AfaucetSdk } from '@zcircuit/afaucet'; // Use this import to test published artifacts
import { DripSdk } from '../../sdk/lib'; // Use this import for local dev
import { algos, expectedRewards, IUtils } from "./utils";

const utils = new IUtils();
const oneHundredPercent = 1000000n;
const testAsaTotalSupply = 10e6;

describe('Claim limits', function () {

    let accounts: Account[];
    let masterAccount: Account;
    let faucetAppId: bigint;
    let faucetAddr: string;
    let sdk: DripSdk;
    let assetId: bigint;

    this.timeout(0);

    async function prepWithDeployOptions(options: DeployConfig) {
        accounts = await utils.createFundedAccounts(3);
        masterAccount = accounts.slice(-1)[0];

        [assetId] = await utils.createTestingASAs([masterAccount]);
        faucetAppId = BigInt(await utils.deployFaucet(masterAccount, assetId, masterAccount.addr, options));
        faucetAddr = algosdk.getApplicationAddress(faucetAppId);
        await utils.sendAlgos(masterAccount, faucetAddr, algos(1));

        sdk = new DripSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            faucetAppId,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
            assetId,
        );

        await sdk.triggerOptInToAsset(masterAccount.addr);
        await utils.sendAsset(assetId, masterAccount, faucetAddr, 10e6);
    }

    it('should limit how long an account can accumulate rewards between claims', async () => {
        await prepWithDeployOptions({
            rewardRate: oneHundredPercent, // 100% of reserve rewarded every second
            maxClaimAccumulationSeconds: 1n,
            maxClaimPercent: oneHundredPercent,
        });

        const addr = accounts[0].addr;
        await sdk.approveAccount(masterAccount.addr, addr);
        const optInConfirmation = await sdk.optIn(addr);

        assert.equal(await utils.lookupBalance(addr, assetId), 0);

        // Claim
        const claimConfirmation = await sdk.claim(addr);
        await utils.waitForAlgodIndexerSync();

        const [optInTime, claimTime] = (await utils.getTxConfirmationTimestamps(optInConfirmation, claimConfirmation))
            .map(BigInt);

        const expectedAssetBalance = expectedRewards(
            optInTime, optInTime + 1n, // Note: capped at 1s regardless of claimTime
            1n, 1n,
            BigInt(10e6), oneHundredPercent,
        );
        const firstClaimAssetBalance = await utils.lookupBalance(addr, assetId);

        assert.equal(firstClaimAssetBalance, Number(expectedAssetBalance));
    });

    it('should limit a claim that exceeds a percentage of the current reserve', async () => {
        await prepWithDeployOptions({
            rewardRate: oneHundredPercent,
            maxClaimAccumulationSeconds: 60n,
            maxClaimPercent: 1n,
        });

        const addr = accounts[0].addr;
        await sdk.approveAccount(masterAccount.addr, addr);
        await sdk.optIn(addr);
        assert.equal(await utils.lookupBalance(addr, assetId), 0);

        // Claim
        await sdk.claim(addr);
        await utils.waitForAlgodIndexerSync();

        // Note time doesn't matter, we'll be limited to one millionth of the reserve
        const expectedAssetBalance = (1n * BigInt(testAsaTotalSupply)) / oneHundredPercent;
        const firstClaimAssetBalance = await utils.lookupBalance(addr, assetId);

        assert.equal(firstClaimAssetBalance, Number(expectedAssetBalance));
    });
});

// TODO: Some safety checks to protect against an overflow error when calculating rewards

type ArgumentTypes<F extends Function> = F extends (...args: infer A) => any ? A : never;
type DeployConfig = ArgumentTypes<typeof utils.deployFaucet>[3]
