import algosdk, { Account, OnApplicationComplete } from "algosdk";
import { assert } from "chai";
// import { AfaucetSdk } from '@zcircuit/afaucet'; // Use this import to test published artifacts
import { DripSdk } from '../../sdk/lib'; // Use this import for local dev
import { algos, assertRejected, expectedRewards, IUtils, textToUint8, tick } from "./utils";

const utils = new IUtils();
const oneHundredPercent = 1000000n;
const globalRewardRate = (oneHundredPercent) / 1000n; // .1%

describe('When Faucet asset funds claimed', function () {

    let accounts: Account[];
    let masterAccount: Account;
    let faucetAppId: bigint;
    let faucetAddr: string;
    let sdk: DripSdk;
    let assetId: bigint;

    this.timeout(0);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);
        masterAccount = accounts.slice(-1)[0];

        [assetId] = await utils.createTestingASAs([masterAccount]);
        faucetAppId = BigInt(await utils.deployFaucet(masterAccount, assetId, masterAccount.addr, {
            rewardRate: globalRewardRate,
            maxClaimAccumulationSeconds: 60n,
            maxClaimPercent: oneHundredPercent,
        }));
        faucetAddr = algosdk.getApplicationAddress(faucetAppId);
        await utils.sendAlgos(masterAccount, faucetAddr, algos(1));

        sdk = new DripSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            faucetAppId,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
            assetId,
        );

        await sdk.triggerOptInToAsset(masterAccount.addr);
        await utils.sendAsset(assetId, masterAccount, faucetAddr, 10e6);
    });

    it('should enable a single user to claim their faucet funds at varying intervals', async () => {
        const addr = accounts[0].addr;
        await sdk.approveAccount(masterAccount.addr, addr);
        const optInConfirmation = await sdk.optIn(addr);

        assert.equal(await utils.lookupBalance(addr, assetId), 0);

        // First Claim
        await tick(10000);
        const claimConfirmation = await sdk.claim(addr);
        await utils.waitForAlgodIndexerSync();

        const [optInTime, claimTime] = (await utils.getTxConfirmationTimestamps(optInConfirmation, claimConfirmation))
            .map(BigInt);

        const expectedAssetBalance = expectedRewards(
            optInTime, claimTime,
            1n, 1n,
            BigInt(10e6), globalRewardRate,
        );
        const firstClaimAssetBalance = await utils.lookupBalance(addr, assetId);

        assert.equal(firstClaimAssetBalance, Number(expectedAssetBalance));

        // Second Claim
        await tick(15000);
        const claimConfirmation2 = await sdk.claim(addr);
        await utils.waitForAlgodIndexerSync();

        const [claimTime2] = (await utils.getTxConfirmationTimestamps(claimConfirmation2)).map(BigInt);

        const expectedAssetRewards2 = expectedRewards(
            claimTime, claimTime2,
            1n, 1n,
            BigInt(10e6 - firstClaimAssetBalance), globalRewardRate,
        );
        const expectedAssetBalance2 = BigInt(firstClaimAssetBalance) + expectedAssetRewards2;
        const secondClaimAssetBalance = await utils.lookupBalance(addr, assetId);

        assert.equal(secondClaimAssetBalance, Number(expectedAssetBalance2));
    });

    it('should enable a multiple users to claim their faucet funds at varying intervals', async () => {
        const addr1 = accounts[0].addr;
        const addr2 = accounts[1].addr;

        const optInConfirmation1 = await sdk.approveAccount(masterAccount.addr, addr1).then(() => sdk.optIn(addr1));
        assert.equal(await utils.lookupBalance(addr1, assetId), 0);

        const optInConfirmationP2 = await sdk.approveAccount(masterAccount.addr, addr2).then(() => sdk.optIn(addr2));
        assert.equal(await utils.lookupBalance(addr2, assetId), 0);

        await tick(10000);

        // P1 - Claim 1
        const p1ClaimConfirmation = await sdk.claim(addr1);
        await utils.waitForAlgodIndexerSync();

        const [optInTime1, p1ClaimTime1] = (await utils.getTxConfirmationTimestamps(optInConfirmation1, p1ClaimConfirmation))
            .map(BigInt);

        const expectedAssetRewards1 = expectedRewards(
            optInTime1, p1ClaimTime1,
            1n, 2n,
            BigInt(10e6), globalRewardRate,
        );
        const firstClaimAssetBalance = await utils.lookupBalance(addr1, assetId);
        console.log(`[P1 - Claim 1] 0 + ${expectedAssetRewards1} = ${firstClaimAssetBalance}`);
        assert.equal(firstClaimAssetBalance, Number(expectedAssetRewards1));
        assert.equal(10e6 - Number(expectedAssetRewards1), await utils.lookupBalance(faucetAddr, assetId)); // sanity check

        // P1 - Claim 2
        await tick(15000);
        const p1ClaimConfirmation2 = await sdk.claim(addr1);
        await utils.waitForAlgodIndexerSync();

        const [p1ClaimTime2] = (await utils.getTxConfirmationTimestamps(p1ClaimConfirmation2)).map(BigInt);

        const expectedAssetRewards2 = expectedRewards(
            p1ClaimTime1, p1ClaimTime2,
            2n, 2n,
            BigInt(10e6 - firstClaimAssetBalance), globalRewardRate,
        );

        const expectedAssetBalance2 = BigInt(firstClaimAssetBalance) + expectedAssetRewards2;
        const secondClaimAssetBalance = await utils.lookupBalance(addr1, assetId);

        console.log(`[P1 - Claim 2] ${firstClaimAssetBalance} + ${expectedAssetRewards2} = ${expectedAssetBalance2}`);
        assert.equal(secondClaimAssetBalance, Number(expectedAssetBalance2));

        // P2 - Claim 1
        const remainingReserveAfterP1Claims = 10e6 - Number(expectedAssetRewards1) - Number(expectedAssetRewards2);
        const actualReserve = await utils.lookupBalance(faucetAddr, assetId);
        assert.equal(remainingReserveAfterP1Claims, actualReserve); // sanity check

        await tick(5000);
        const p2ClaimConfirmation1 = await sdk.claim(addr2);
        await utils.waitForAlgodIndexerSync();

        const [p2OptInTime, p2ClaimTime1] = (await utils.getTxConfirmationTimestamps(optInConfirmationP2, p2ClaimConfirmation1)).map(BigInt);

        const expectedAssetRewards3 = expectedRewards(
            p2OptInTime, p2ClaimTime1,
            2n, 2n,
            BigInt(remainingReserveAfterP1Claims), globalRewardRate,
        );
        const claimAssetBalance3 = await utils.lookupBalance(addr2, assetId);
        console.log(`[P2 - Claim 1] 0 + ${expectedAssetRewards3} = ${claimAssetBalance3}`);
        assert.equal(claimAssetBalance3, Number(expectedAssetRewards3));
    });

    it('should pay claims with innerTxn fee set to zero (relying on fee pooling)', async () => {
        const addr = accounts[0].addr;
        await sdk.approveAccount(masterAccount.addr, addr);
        await sdk.optIn(addr);

        const suggestedParams = await utils.algod.getTransactionParams().do();
        const tx = algosdk.makeApplicationCallTxnFromObject({
            suggestedParams,
            from: addr,
            appIndex: Number(faucetAppId),
            onComplete: OnApplicationComplete.NoOpOC,
            appArgs: [textToUint8('claim')],
            foreignAssets: [Number(assetId)]
        });
        const signedTx = tx.signTxn(accounts[0].sk);
        assertRejected(utils.algod.sendRawTransaction(signedTx).do(), e =>
            assert.include(e.message, 'logic eval error: fee too small')
        );
    });
});
