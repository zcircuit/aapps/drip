export interface CreateApplicationTransactionQuery {
    "current-round": number,
    "transaction": {
        "application-transaction": {
            "accounts": [],
            "application-args": [],
            "application-id": number,
            "approval-program": string,
            "clear-state-program": string,
            "foreign-apps": [],
            "foreign-assets": [],
            "global-state-schema": {
                "num-byte-slice": number,
                "num-uint": number
            },
            "local-state-schema": {
                "num-byte-slice": number,
                "num-uint": number
            },
            "on-completion": string
        },
        "close-rewards": number,
        "closing-amount": number,
        "confirmed-round": number,
        "created-application-index": number,
        "fee": number,
        "first-valid": number,
        "genesis-hash": string,
        "genesis-id": string,
        "global-state-delta": [
            {
                "key": string,
                "value": {
                    "action": number,
                    "bytes": string,
                    "uint": number
                }
            }
        ],
        "id": string,
        "intra-round-offset": number,
        "last-valid": number,
        "receiver-rewards": number,
        "round-time": number,
        "sender": string,
        "sender-rewards": number,
        "signature": {
            "sig": string
        },
        "tx-type": string
    }
}