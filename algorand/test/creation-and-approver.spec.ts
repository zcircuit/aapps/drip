import algosdk, { Account } from "algosdk";
import { assert } from "chai";
// import { AfaucetSdk } from '@zcircuit/afaucet'; // Use this import to test published artifacts
import { DripSdk, DripTransactions } from '../../sdk/lib'; // Use this import for local dev
import { algos, assertRejected, IUtils, textToUint8 } from "./utils";

const utils = new IUtils();
describe('Faucet Creation and Approver', function () {

    let accounts: Account[];
    let masterAccount: Account;
    let faucetAppId: bigint;
    let faucetAddr: string;
    let sdk: DripSdk;
    let assetId: bigint;

    this.timeout(0);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);
        masterAccount = accounts.slice(-1)[0];

        [assetId] = await utils.createTestingASAs([masterAccount]);
        faucetAppId = BigInt(await utils.deployFaucet(masterAccount, assetId, masterAccount.addr));
        faucetAddr = algosdk.getApplicationAddress(faucetAppId);
        await utils.sendAlgos(masterAccount, faucetAddr, algos(1));

        sdk = new DripSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            faucetAppId,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
            assetId,
        );
    });

    describe('aoptin()', () => {

        it('should opt-in to the reserve asset', async () => {
            await utils.sendAlgos(accounts[0], faucetAddr, algos(.2));

            await sdk.triggerOptInToAsset(masterAccount.addr);
            await utils.waitForAlgodIndexerSync();

            const response = await utils.indexer.lookupAccountAssets(faucetAddr).do();
            const assetsFaucetHasOptIn: number[] = response.assets.map((item: any) => item['asset-id']);
            assert.includeMembers(assetsFaucetHasOptIn, [Number(assetId)]);
        });

        it('should explicitly set the innerTxn fee to zero', async () => {
            await utils.sendAlgos(accounts[0], faucetAddr, algos(.2));

            await assertRejected(sdk.triggerOptInToAsset(masterAccount.addr, 1e3), e => {
                assert.include(e.message, 'fee too small');
            });
        });
    });

    describe('user opt-in', () => {

        beforeEach(async () => {
            await utils.sendAlgos(accounts[0], faucetAddr, algos(.2));
            await sdk.triggerOptInToAsset(masterAccount.addr);
        });

        describe('using Approver to allowlist', () => {
            it('should prevent an opt-in if the user isnt optedin to the faucet asset', async () => {
                await sdk.approveAccount(masterAccount.addr, accounts[0].addr);

                const suggestedParams = await utils.algod.getTransactionParams().do();
                const tx = algosdk.makeApplicationOptInTxnFromObject({
                    suggestedParams,
                    from: accounts[0].addr,
                    appIndex: Number(faucetAppId),
                });
                const sentTxn = utils.algod.sendRawTransaction(tx.signTxn(accounts[0].sk)).do();
                await assertRejected(sentTxn, e => assert.include(e.message, 'asset_holding_get AssetBalance'));
            });

            it('should allow the Approver to pre-allowlist an account', async () => {
                await sdk.approveAccount(masterAccount.addr, accounts[0].addr);
                await sdk.optIn(accounts[0].addr);
                await utils.waitForAlgodIndexerSync();

                const accountResponse = await utils.indexer.lookupAccountByID(accounts[0].addr).do();
                const apps: bigint[] = accountResponse.account['apps-local-state'].map((ls: any) => BigInt(ls.id));

                assert.includeMembers(apps, [faucetAppId]);
            });

            it('should allow the Approver to sequentially allowlist accounts', async () => {
                await sdk.approveAccount(masterAccount.addr, accounts[0].addr);
                await sdk.optIn(accounts[0].addr);

                await sdk.approveAccount(masterAccount.addr, accounts[1].addr);
                await sdk.optIn(accounts[1].addr);

                await utils.waitForAlgodIndexerSync();

                const accountResponse1 = await utils.indexer.lookupAccountByID(accounts[0].addr).do();
                const apps1: bigint[] = accountResponse1.account['apps-local-state'].map((ls: any) => BigInt(ls.id));

                const accountResponse2 = await utils.indexer.lookupAccountByID(accounts[1].addr).do();
                const apps2: bigint[] = accountResponse2.account['apps-local-state'].map((ls: any) => BigInt(ls.id));

                assert.includeMembers(apps1, [faucetAppId]);
                assert.includeMembers(apps2, [faucetAppId]);
            });

            it('should allow just a single account to be allowlisted at a time', async () => {
                await sdk.approveAccount(masterAccount.addr, accounts[0].addr);
                await sdk.approveAccount(masterAccount.addr, accounts[1].addr);

                await assertRejected(sdk.optIn(accounts[0].addr));
            });

            it('should reject an account attempting to optin without being allowlisted', async () => {
                await assertRejected(sdk.optIn(accounts[0].addr));
            });

            it('should reject an incorrect approver attempting to allowlist', async () => {
                await assertRejected(sdk.approveAccount(accounts[0].addr, accounts[1].addr));
            });
        });

        describe('using Approver in a group txn', () => {

            let faucetTxns: DripTransactions;

            beforeEach(async () => {
                faucetTxns = new DripTransactions(algosdk, faucetAppId, assetId);
                await utils.optInAssetIfNecessary(accounts[0], assetId);
            });

            it('should approve a user when an additional txn from the approver is included before the user opt-in txn', async () => {
                const suggestedParams = await utils.algod.getTransactionParams().do();
                const txns = [];

                txns.push(algosdk.makeApplicationNoOpTxnFromObject({
                    from: masterAccount.addr,
                    appIndex: Number(faucetAppId),
                    suggestedParams,
                    appArgs: [
                        textToUint8('allowlist'),
                        algosdk.decodeAddress(accounts[0].addr).publicKey,
                    ],
                }));
                txns.push(faucetTxns.optInToDrip(accounts[0].addr, suggestedParams));

                const groupedTxns = algosdk.assignGroupID(txns);
                const signedTxns = [
                    groupedTxns[0].signTxn(masterAccount.sk),
                    groupedTxns[1].signTxn(accounts[0].sk),
                ];

                await utils.algod.sendRawTransaction(signedTxns).do();
            });
        })
    });
});

// >> Putting this in the backlog, we don't currently have a need but if we find its usefull to other projects we could revisit

    // What if I don't want an approver

    // describe('using Approver delegation', () => {
    // it('should be able to delegate the Approver', async () => {
    //     assert.ok(true);
    // });

    // it('when delegating Approver should set the fee of the innerTxn to zero (relies on fee pooling)', async () => {
    //     assert.ok(true);
    // });
    // });
