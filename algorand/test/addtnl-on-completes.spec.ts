import algosdk, { Account } from "algosdk";
import { assert } from "chai";
// import { DripSdk } from '@zcircuit/drip'; // Use this import to test published artifacts
import { DripSdk } from '../../sdk/lib'; // Use this import for local dev
import { algos, assertRejected, b64ToUint8, expectedRewards, IUtils, tick } from "./utils";

const utils = new IUtils();
const oneHundredPercent = 1000000n;
const globalRewardRate = (oneHundredPercent) / 1000n; // .1%

describe(`Faucet Additional OnComplete's`, function () {

    let accounts: Account[];
    let masterAccount: Account;
    let faucetAppId: bigint;
    let faucetAddr: string;
    let sdk: DripSdk;
    let assetId: bigint;
    let optInConfirmation: any;

    this.timeout(0);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);
        masterAccount = accounts.slice(-1)[0];

        [assetId] = await utils.createTestingASAs([masterAccount]);
        faucetAppId = BigInt(await utils.deployFaucet(masterAccount, assetId, masterAccount.addr, {
            rewardRate: globalRewardRate,
            maxClaimAccumulationSeconds: 60n,
            maxClaimPercent: oneHundredPercent,
        }));
        faucetAddr = algosdk.getApplicationAddress(faucetAppId);
        await utils.sendAlgos(masterAccount, faucetAddr, algos(.2));

        sdk = new DripSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            faucetAppId,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
            assetId,
        );
        await sdk.triggerOptInToAsset(masterAccount.addr);
        await utils.sendAsset(assetId, masterAccount, faucetAddr, 10e6);

        await sdk.approveAccount(masterAccount.addr, accounts[0].addr);
        optInConfirmation = await sdk.optIn(accounts[0].addr);
    });

    it('should reject OnComplete.Update', async () => {
        const noOpProgram = '#pragma version 6\nint 1';
        const compiledNoOpProgram = await utils.algod.compile(noOpProgram).do();

        const suggestedParams = await utils.algod.getTransactionParams().do();
        const tx = algosdk.makeApplicationUpdateTxnFromObject({
            appIndex: Number(faucetAppId),
            approvalProgram: b64ToUint8(compiledNoOpProgram.result),
            clearProgram: b64ToUint8(compiledNoOpProgram.result),
            from: masterAccount.addr,
            suggestedParams
        });

        const p = utils.algod.sendRawTransaction(tx.signTxn(masterAccount.sk)).do();
        await assertRejected(p, e => assert.include(e.message, 'transaction rejected by ApprovalProgram'));
    });

    it('should reject OnComplete.Delete', async () => {
        const suggestedParams = await utils.algod.getTransactionParams().do();
        const tx = algosdk.makeApplicationDeleteTxnFromObject({
            appIndex: Number(faucetAppId),
            from: masterAccount.addr,
            suggestedParams,
        });

        const p = utils.algod.sendRawTransaction(tx.signTxn(masterAccount.sk)).do();
        await assertRejected(p, e => assert.include(e.message, 'transaction rejected by ApprovalProgram'));
    });

    it('should reject OnComplete.CloseOut forcing a ClearState txn to opt-out', async () => {
        const suggestedParams = await utils.algod.getTransactionParams().do();
        const tx = algosdk.makeApplicationCloseOutTxnFromObject({
            appIndex: Number(faucetAppId),
            from: masterAccount.addr,
            suggestedParams,
        });

        const p = utils.algod.sendRawTransaction(tx.signTxn(masterAccount.sk)).do();
        await assertRejected(p, e => assert.include(e.message, 'transaction rejected by ApprovalProgram'));
    });

    it('should decrement the global participants counter when an account does a ClearState txn', async () => {
        const addr1 = accounts[0].addr;
        const addr2 = accounts[1].addr;

        await sdk.approveAccount(masterAccount.addr, addr2).then(() => sdk.optIn(addr2))

        // Make a clearState for addr2
        const suggestedParams = await utils.algod.getTransactionParams().do();
        const clearTx = algosdk.makeApplicationClearStateTxnFromObject({
            appIndex: Number(faucetAppId),
            from: addr2,
            suggestedParams
        });
        await utils.algod.sendRawTransaction(clearTx.signTxn(accounts[1].sk)).do();

        // Claim with addr1 expecting numP to be 1 at beginning and end, as if addr2 never participated
        await tick(10000);
        const claimConfirmation = await sdk.claim(addr1);
        await utils.waitForAlgodIndexerSync();

        const [optInTime, claimTime] = (await utils.getTxConfirmationTimestamps(optInConfirmation, claimConfirmation))
            .map(BigInt);

        const expectedAssetBalance = expectedRewards(
            optInTime, claimTime,
            1n, 1n,
            BigInt(10e6), globalRewardRate,
        );

        const firstClaimAssetBalance = await utils.lookupBalance(addr1, assetId);

        assert.equal(firstClaimAssetBalance, Number(expectedAssetBalance));
    });

});
