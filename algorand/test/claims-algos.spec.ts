import algosdk, { Account } from "algosdk";
import { assert } from "chai";
// import { AfaucetSdk } from '@zcircuit/afaucet'; // Use this import to test published artifacts
import { DripSdk } from '../../sdk/lib'; // Use this import for local dev
import { algos, expectedRewards, fees, IUtils, tick } from "./utils";

const utils = new IUtils();
const oneHundredPercent = 1000000n;
const globalRewardRate = (oneHundredPercent) / 1000n; // .1%
const accountsStartingAlgoBalance = algos(10);
const faucetStartingBalance = accountsStartingAlgoBalance - algos(1);

describe('When Faucet algo funds claimed', function () {

    let accounts: Account[];
    let masterAccount: Account;
    let faucetAppId: bigint;
    let faucetAddr: string;
    let sdk: DripSdk;
    const assetId = 1n;

    this.timeout(0);

    beforeEach(async () => {
        accounts = await utils.createFundedAccounts(3);
        masterAccount = accounts.slice(-1)[0];

        faucetAppId = BigInt(await utils.deployFaucet(masterAccount, assetId, masterAccount.addr, {
            rewardRate: globalRewardRate,
            maxClaimAccumulationSeconds: 60n,
            maxClaimPercent: oneHundredPercent,
        }));
        faucetAddr = algosdk.getApplicationAddress(faucetAppId);
        await utils.sendAlgos(masterAccount, faucetAddr, faucetStartingBalance);

        sdk = new DripSdk(
            algosdk,
            utils.algod,
            utils.indexer,
            faucetAppId,
            async (txns) => txns.map(tx =>
                tx.signTxn(accounts.find(account =>
                    account.addr === algosdk.encodeAddress(tx.from.publicKey))!.sk)),
            assetId,
        );
    });

    it('should enable a single user to claim their faucet algos', async () => {
        const addr = accounts[0].addr;

        await sdk.approveAccount(masterAccount.addr, addr);
        const optInConfirmation = await sdk.optIn(addr);

        assert.closeTo(await utils.lookupBalance(addr), accountsStartingAlgoBalance - fees(1), 999);

        // First Claim
        await tick(10000);
        const claimConfirmation = await sdk.claim(addr);
        await utils.waitForAlgodIndexerSync();

        const [optInTime, claimTime] = (await utils.getTxConfirmationTimestamps(optInConfirmation, claimConfirmation))
            .map(BigInt);

        const expectedFaucetMinBalance = algos(.1);
        const expectedAssetBalance = expectedRewards(
            optInTime, claimTime,
            1n, 1n,
            BigInt(faucetStartingBalance - expectedFaucetMinBalance), globalRewardRate,
        );
        const firstClaimAssetBalance = await utils.lookupBalance(addr);

        assert.closeTo(firstClaimAssetBalance, Number(expectedAssetBalance) + accountsStartingAlgoBalance + (-fees(3)), 999);
    });
});
